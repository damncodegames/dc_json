﻿using System.Collections;

namespace DC.JSON
{
	using System;
	using System.Collections.Generic;
	using System.Reflection;

	public static class GenericAdapter
	{
		//--------------------------------------------------
		// Special fields
		// In order: (Static/Enums/Constants)
		//--------------------------------------------------
		
		#region JSON_FROM_OBJECT
		public static JSONObject JSONFrom<T>(T genericObj) where T : class
		{
			if(genericObj == null)
				return null;

			// Type of the object passed as argument
			Type type = genericObj.GetType();

			// All the fields of the object passed as argument
			IEnumerable<PropertyInfo> properties = type.GetProperties();//type.GetFilteredProperties(new List<Type> { typeof(SkipPropertyAttribute), typeof(GetObjectIDAttribute) });
			
			JSONObject jsonObj = new JSONObject();
			
			foreach (PropertyInfo property in properties) 
			{
				Type propType = property.PropertyType;
				string propName = property.Name;
				object propertyValue = property.GetValue(genericObj, null);

				if(propType.IsPrimitive)
				{
					AddPrimitiveToJSON(jsonObj, propType, propName, propertyValue);
				}
				else if (propType == typeof(string)) 
				{
					jsonObj.AddField(property.Name, (string)propertyValue);
				}
				else if (propType == typeof(DateTime)) 
				{
					DateTime dateTime = (DateTime)propertyValue;
					jsonObj.AddField(property.Name, dateTime.ToString(System.Globalization.CultureInfo.InvariantCulture));
				}
				else if (propType.IsArray && (propertyValue != null))
				{
					Array array = (Array)propertyValue;
					JSONObject arrayJSON = JSONFromArray(array);
					jsonObj.AddField (propName, arrayJSON);
				}
				else if (propType.IsGenericType)
				{
					// Type of the generic property
					var listType = propType.GetGenericTypeDefinition ();
					
					// We check if it is a generic list
					if (listType == typeof(List<>))
					{
						IList list = (IList)propertyValue;
						JSONObject arrayJSON = JSONFromList(list);
						jsonObj.AddField (propName, arrayJSON);
					}
				}
				else
				{
					// Information to call JSONFrom method recursively with the appropriate type
					MethodInfo genericMethod = GetGenericMethod("JSONFrom", propType);
					var objJSON = (JSONObject)genericMethod.Invoke(null, new object[]{ propertyValue });
					jsonObj.AddField(propName, objJSON);
				}
			}
			
			return jsonObj;
		}

		private static JSONObject JSONFromArray (Array array)
		{
			JSONObject arrayJSON = new JSONObject(JSONObject.Type.ARRAY);

			for(int i = 0; i< array.Length; i++)
			{
				object obj = array.GetValue(i);
				AddToArrayJSON (arrayJSON, obj);
			}

			return arrayJSON;
		}

		private static JSONObject JSONFromList (IList list)
		{
			JSONObject arrayJSON = new JSONObject(JSONObject.Type.ARRAY);

			for(int i = 0; i< list.Count; i++)
			{
				object obj = list[i];
				AddToArrayJSON (arrayJSON, obj);
			}

			return arrayJSON;
		}

		private static void AddToArrayJSON (JSONObject arrayJSON, object obj)
		{
			Type objType = obj.GetType ();
			if (objType == typeof(bool))
			{
				arrayJSON.Add ((bool) obj);
			}
			else if ((objType == typeof(int)) || (objType == typeof (uint)))
			{
				arrayJSON.Add ((int) obj);
			}
			else if ((objType == typeof(float)) || (objType == typeof (double)))
			{
				arrayJSON.Add ((float) obj);
			}
			else if (objType == typeof(string))
			{
				arrayJSON.Add ((string) obj);
			}
			else if (objType == typeof (DateTime))
			{
				arrayJSON.Add (obj.ToString ());
			}
			else
			{
				// Information to call JSONFrom method recursively with the appropriate type
				MethodInfo genericMethod = GetGenericMethod("JSONFrom", objType);
				var objJSON = (JSONObject)genericMethod.Invoke(null, new object[]{ obj });
				arrayJSON.Add (objJSON);
			}
		}

		private static void AddPrimitiveToJSON(JSONObject jsonObj, Type propType, string propertyName, object propertyValue)
		{
			if (propType == typeof(int)) 
			{
				jsonObj.AddField(propertyName, (int)propertyValue);
			}
			if (propType == typeof(uint)) 
			{
				jsonObj.AddField(propertyName, (uint)propertyValue);
			}
			else if (propType == typeof(float)) 
			{
				jsonObj.AddField(propertyName, (float)propertyValue);
			}
			else if (propType == typeof(double)) 
			{
				jsonObj.AddField(propertyName, (float)(double)propertyValue);
			}
			else if (propType == typeof(bool)) 
			{
				jsonObj.AddField(propertyName, (bool)propertyValue);
			}
		}
		#endregion

		#region OBJECT_FROM_JSON
		public static T FromJSON<T> (string jsonText) where T : class, new()
		{
			return FromJSON<T> (new JSONObject (jsonText));
		}

		public static T FromJSON<T> (JSONObject jsonObj) where T : class, new()
		{
			T genericObj = new T();
			
			return FromJSON <T> (genericObj, jsonObj);
		}

		public static T FromJSON<T> (T genericObj, JSONObject jsonObj) where T : class, new()
		{
			Type type = genericObj.GetType();//typeof (T);

			// We get the properties of the generic object
			var properties = type.GetProperties(); //type.GetFilteredProperties(new List<Type> { typeof(GetObjectIDAttribute) });
			foreach (PropertyInfo property in properties)
			{
				Type propType = property.PropertyType;
				string propName = property.Name;

				// Primitive types
				if (propType.IsPrimitive)
				{
					// int
					if (propType == typeof (System.Int32))
					{
						//Debug.Log ("Property name: "+propName);

						var fieldJSON = jsonObj.GetField(propName);
						if (fieldJSON != null)
						{
							int integerProperty = (int)fieldJSON.n;
							property.SetValue (genericObj, integerProperty, null);	
						}
					}
					// float
					else if (propType == typeof (System.Single))
					{
						var fieldJSON = jsonObj.GetField(propName);
						if (fieldJSON != null)
						{
							float floatProperty = fieldJSON.f;
							property.SetValue (genericObj, floatProperty, null);
						}
					}
					// double
					else if (propType == typeof (System.Double))
					{
						var fieldJSON = jsonObj.GetField(propName);
						if (fieldJSON != null)
						{
							double doubleProperty = (double)fieldJSON.n;
							property.SetValue (genericObj, doubleProperty, null);
						}
					}
					// bool
					else if (propType == typeof (System.Boolean))
					{
						var fieldJSON = jsonObj.GetField(propName);
						if (fieldJSON != null)
						{
							bool boolProperty = fieldJSON.b;
							property.SetValue (genericObj, boolProperty, null);
						}
					}
				}
				// String type
				else if (propType == typeof (System.String))
				{
					var fieldJSON = jsonObj.GetField(propName);
					if (fieldJSON != null)
					{
						string stringProperty = fieldJSON.str;
						property.SetValue (genericObj, stringProperty, null);
					}
				}
				// Date type
				else if (propType == typeof(System.DateTime))
				{
					var fieldJSON = jsonObj.GetField(propName);
					if (fieldJSON != null)
					{
						string dateTimeProperty = fieldJSON.str;
						DateTime date = DateTime.Parse(dateTimeProperty, System.Globalization.CultureInfo.InvariantCulture);
						property.SetValue (genericObj, date, null);
					}
				}
				// Array type
				else if (propType.IsArray)
				{
					var fieldJSON = jsonObj.GetField(propName);

					if (fieldJSON != null)
					{
						Array array = FromJSONArray(propType, fieldJSON);
						property.SetValue (genericObj, array, null);
					}
				}
				// Generic type
				else if (propType.IsGenericType)
				{
					var fieldJSON = jsonObj.GetField(propName);

					// Type of the generic property
					Type listType = propType.GetGenericTypeDefinition ();

					// We check if it is a generic list
					if (fieldJSON != null && listType == typeof(List<>))
					{
						var newList = FromJSONGenericList(propType, listType, fieldJSON);
						property.SetValue (genericObj, newList, null);
					}
				}
				else
				{
					var fieldJSON = jsonObj.GetField(propName);
					if (fieldJSON != null)
					{
						// Information to call this method recursively with the appropriate type
						MethodInfo genericMethod = GetGenericMethod("FromJSON", typeof (JSONObject), propType);

						var obj = genericMethod.Invoke(null, new object[] { fieldJSON });
						property.SetValue (genericObj, obj, null);
					}
				}
				//Debug.Log (property.Name+": "+propType);
			}
			//Debug.Log (genericObj);
			
			return genericObj;
		}

		private static Array FromJSONArray(Type propType, JSONObject fieldJSON)
		{
			// Type of the objects contained in the array
			Type arrayElementType = propType.GetElementType();

			// Creation of the array to be added to the object we are creating
			Array array = Array.CreateInstance (propType.GetElementType(), fieldJSON.list.Count);

			// Primitive types
			if (arrayElementType.IsPrimitive)
			{
				// int
				if (arrayElementType == typeof (System.Int32))
				{
					int index = 0;
					foreach (var jsonChild in fieldJSON.list)
					{
						array.SetValue(jsonChild.n, index++);
					}
				}
				// float
				else if (arrayElementType == typeof (System.Single))
				{
					int index = 0;
					foreach (var jsonChild in fieldJSON.list)
					{
						array.SetValue(jsonChild.f, index++);
					}
				}
				// double
				else if (arrayElementType == typeof (System.Double))
				{
					int index = 0;
					foreach (var jsonChild in fieldJSON.list)
					{
						array.SetValue(jsonChild.n, index++);
					}
				}
				// bool
				else if (arrayElementType == typeof (System.Boolean))
				{
					int index = 0;
					foreach (var jsonChild in fieldJSON.list)
					{
						array.SetValue(jsonChild.b, index++);
					}
				}
			}
			else if(arrayElementType == typeof (System.String))
			{
				int index = 0;
				foreach (var jsonChild in fieldJSON.list)
				{
					array.SetValue(jsonChild.str, index++);
				}
			}
			else
			{
				// Information to call this method recursively with the appropriate type
				MethodInfo genericMethod = GetGenericMethod("FromJSON", typeof (JSONObject), arrayElementType);

				// By calling recursively the method we collect the objects to be added to the array
				int index = 0;
				foreach (var jsonChild in fieldJSON.list)
				{
					var obj = genericMethod.Invoke(null, new object[]{jsonChild});
					array.SetValue(obj, index++);
				}
			}
			return array;
		}

		private static IList FromJSONGenericList(Type propType, Type listType, JSONObject fieldJSON)
		{
			// Array types contained in the list
			Type[] genericArguments = propType.GetGenericArguments();

			// Type of the objects contained in the generic list
			Type listElementType = genericArguments[0];

			// Concrete type of the list with the generic argument setted
			Type newTypeList = listType.MakeGenericType(genericArguments);

			// Creation of the generic list to be added to the object we are creating
			var newList = (IList)Activator.CreateInstance(newTypeList);

			// Primitive types
			if (listElementType.IsPrimitive)
			{
				// int
				if (listElementType == typeof (System.Int32))
				{
					foreach (var jsonChild in fieldJSON.list)
					{
						newList.Add(jsonChild.n);
					}
				}
				// float
				else if (listElementType == typeof (System.Single))
				{
					foreach (var jsonChild in fieldJSON.list)
					{
						newList.Add(jsonChild.f);
					}
				}
				// double
				else if (listElementType == typeof (System.Double))
				{
					foreach (var jsonChild in fieldJSON.list)
					{
						newList.Add(jsonChild.n);
					}
				}
				// bool
				else if (listElementType == typeof (System.Boolean))
				{
					foreach (var jsonChild in fieldJSON.list)
					{
						newList.Add(jsonChild.b);
					}
				}
			}
			else if(listElementType == typeof (System.String))
			{
				foreach (var jsonChild in fieldJSON.list)
				{
					newList.Add(jsonChild.str);
				}
			}
			else
			{
				// Information to call this method recursively with the appropriate type
				MethodInfo genericMethod = GetGenericMethod("FromJSON", typeof (JSONObject), listElementType);

				// By calling recursively the method we collect the objects to be added to the generic list
				foreach (var jsonChild in fieldJSON.list)
				{
					var obj = genericMethod.Invoke(null, new object[]{jsonChild});
					newList.Add(obj);
				}
			}

			return newList;
		}

		#endregion

		#region UTILS
		private static MethodInfo GetGenericMethod (string methodName, Type argumentType, Type genericType)
		{
			// Information to call this method recursively with the appropriate type
			MethodInfo method = typeof (GenericAdapter).GetMethod(methodName, new Type[] { argumentType });
			return method.MakeGenericMethod(new Type[] { genericType });
		}

		private static MethodInfo GetGenericMethod (string methodName, Type genericType)
		{
			// Information to call this method recursively with the appropriate type
			MethodInfo method = typeof (GenericAdapter).GetMethod(methodName);
			return method.MakeGenericMethod(new Type[] { genericType });
		}
		#endregion
	}
}