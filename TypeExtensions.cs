﻿using System.Collections.Generic;
using System.Reflection;
using System;
using System.Linq;
using System.Runtime.InteropServices;

[AttributeUsage (AttributeTargets.Property, Inherited = false), ComVisible (true)]
public sealed class SkipPropertyAttribute : Attribute {}

[AttributeUsage (AttributeTargets.Property, Inherited = false), ComVisible (true)]
public sealed class GetObjectIDAttribute : Attribute {}

public static class TypeExtensions 
{
	#region PROPERTIES
	public static IEnumerable<PropertyInfo> GetFilteredProperties(this Type myType, Type filterType){
		return myType.GetProperties().Where(
			pi => !Attribute.IsDefined(pi, filterType)
		);
	}

	public static IEnumerable<PropertyInfo> GetFilteredProperties(this Type myType, List<Type> filterList){
		return myType.GetProperties().Where(
			pi => !filterList.Exists ((type) => Attribute.IsDefined(pi, type))
		);
	}

	public static IEnumerable<PropertyInfo> GetPropertiesByAtrribute(this Type myType, Type type){
		return myType.GetProperties().Where(
			pi => Attribute.IsDefined(pi, type)
		);
	}
	#endregion
}
